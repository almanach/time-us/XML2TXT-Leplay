# -*- coding: utf-8 -*-

def debug_spotting_titles(list_of_pages: list) -> None:
    """Print name of objects in the provided list identified as having a title."""
    title_pages = [page for page in list_of_pages if page.has_title is True]
    if len(title_pages) == 0:
        print("[DEBUG]: There was no page containing a title in this series of pages.")
    else:
        print("[DEBUG]: The following pages were identified as containing a title:")
        for title_page in title_pages:
            print(title_page.name)


def debug_building_chapter(list_of_chapters: list) -> None:
    """Print the name of the first image of each chapter given in provided the list."""
    print("[DEBUG]: got {} slice(s) out of this volume starting as follows:".format(len(list_of_chapters)))
    for section in list_of_chapters:
        if len(section.content) > 0:
            print(section.content[0].name)


def debug_how_many_chapters(list_of_chapters: list) -> None:
    """Print numbers of chapter in the list of chapters."""
    print("[DEBUG]: there are {} chapters".format(len(list_of_chapters)))