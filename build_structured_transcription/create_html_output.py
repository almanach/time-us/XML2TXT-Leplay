# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import os
import argparse

parser = argparse.ArgumentParser(description="Transform a TEI XML file to publish it as HTML.")
parser.add_argument('-i', '--input', action='store', required=True, nargs=1, help='Path to the file to transform.')
args = parser.parse_args()

with open(args.input[0], 'r') as fh:
    file_content = fh.read()
parsed_file = BeautifulSoup(file_content, 'xml')

temporary_title = 'Les Ouvriers des Deux Mondes - série 1, volume 1'

# change link to images for demo
images = [image for image in parsed_file.find_all('graphic') if 'url' in image.attrs]
for image in images:
    image['url'] = image['url'].replace('.tif', '.png')
    image['url'] = 'images/{}'.format(image['url'].split(os.sep)[-1])

# create HTML basis:
html_basis = """<html xmlns="http://www.w3.org/1999/xhtml"><head><title>{}</title><meta charset="UTF-8"></head><body></body></html>""".format(temporary_title)
parsed_html_basis = BeautifulSoup(html_basis, 'lxml')

# create an index div
title = BeautifulSoup("""<h1>{}</h1>""".format(temporary_title), 'lxml')
index = BeautifulSoup("""<div temp_class="index"><h2>Table des matières</h2><ul></ul></div><hr align="center" width="100%" size="4" color="#E0115F">""", 'lxml')
parsed_html_basis.body.append(title.h1)
parsed_html_basis.body.append(index.div)

# add every new chapter in HTML file
counter_chapter = 1
for chapter in parsed_file.find_all('div', type='chapter'):
    # HTMLize this tree
    chapter['id'] = chapter['xml:id']
    chapter['class'] = chapter['type']
    del chapter['xml:id']
    del chapter['type']
    # add entry for this chapter in index
    chapter_toc_entry = BeautifulSoup("""<li><a href="#{}">Chapitre {} : {}...</a></li>""".format(chapter['id'], counter_chapter,chapter.get_text()[:100]), 'lxml')

    if len(chapter.find_all('head', type='section')) > 0:
        chapter_toc_entry.append(parsed_html_basis.new_tag('ul'))
        for section in chapter.find_all('div', type='section'):
            section['class'] = section['type']
            del section['type']
            section_title = section.head
            section_title.name = 'h3'
            section_title['id'] = section_title['xml:id']
            del section_title['xml:id']
            section_toc_entry = BeautifulSoup(
                """<li><a href="#{}">{}</a></li>""".format(section_title['id'], section_title.string), 'lxml')

            if len(section.find_all('div', type='sub_section')) > 0:
                section_toc_entry.append(parsed_html_basis.new_tag('ul'))
                for sub_section in section.find_all('div', type='sub_section'):
                    sub_section['class'] = sub_section['type']
                    del sub_section['type']
                    if len(sub_section.find_all('head', type='sub_section')):
                        for sub_section_title in sub_section.find_all('head', type='sub_section'):
                            #sub_section_title = sub_section.head
                            sub_section_title.name = 'h4'
                            sub_section_title['id'] = sub_section_title['xml:id']
                            del sub_section_title['xml:id']
                            sub_section_toc_entry = BeautifulSoup(
                                """<li><a href="#{}">{}</a></li>""".format(sub_section_title['id'], sub_section_title.string), 'lxml')
                            if len(sub_section.find_all('div', type='sub_sub_section')) > 0:
                                sub_section_toc_entry.append(parsed_html_basis.new_tag('ul'))
                                for sub_sub_section in sub_section.find_all('div', type='sub_sub_section'):
                                    sub_sub_section['class'] = sub_sub_section['type']
                                    del sub_sub_section['type']
                                    if len(sub_sub_section.find_all('head', type='sub_sub_section')):
                                        for sub_sub_section_title in sub_sub_section.find_all('head', type='sub_sub_section'):
                                            sub_sub_section_title.name = "h5"
                                            sub_sub_section_title['id'] = sub_sub_section_title['xml:id']
                                            del sub_sub_section_title['xml:id']
                                            sub_sub_section_toc_entry = BeautifulSoup(
                                                """<li><a href="#{}">{}</a></li>""".format(sub_sub_section_title['id'], sub_sub_section_title.string), 'lxml')
                                            sub_section_toc_entry.ul.append(sub_sub_section_toc_entry)
                            section_toc_entry.ul.append(sub_section_toc_entry)
            chapter_toc_entry.ul.append(section_toc_entry)
    parsed_html_basis.find_all('div', temp_class='index')[0].ul.append(chapter_toc_entry.li)
    parsed_html_basis.body.append(chapter)
    # add a visual separation before next chapter div
    parsed_html_basis.body.append(parsed_html_basis.new_tag('hr', align='center', width='50%', size='3', color='#002444'))
    counter_chapter += 1

# remplacer les <graphic type='table"> par <a href='{$image_de_la_page} target="_BLANK"><img src="{$logo_table}></a>
for table in parsed_html_basis.body.find_all('graphic', type='table'):
    table.name = 'img'
    table['src'] = 'https://img.icons8.com/windows/420/table-1.png'
    table['width'] = "60"
    table['alt'] = 'une icone représentant une tableau'
    table['title'] = 'cliquer pour afficher le tableau'
    page_image = [z for z in parsed_file.find_all('zone', rendition="table") if z['xml:id'] == table['facs'][1:]][0].parent.parent.graphic['url']
    table.wrap(parsed_html_basis.new_tag('a', target='_BLANK', href=page_image)).wrap(parsed_html_basis.new_tag('p'))

# renommer les tag <pb/> en <br type="page_beginning">
for pb in parsed_html_basis.body.find_all('pb'):
    pb.name = 'br'
    pb['type'] = 'page_beg'


# ajouter un bouton de retour rapide vers le haut de page
button_style = """style="position:fixed;bottom:20px;right:30px;z-index:99;padding:5px;font-size:14px;" """
button = BeautifulSoup("""<button onclick="topFunction()" id="myBtn" title="Retour vers le haut de page" {}>Haut</button>""".format(button_style), 'xml')
button_javascript_content = """
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}"""
button_javascript_tag = BeautifulSoup("<script>{}</script>".format(button_javascript_content), 'lxml')
parsed_html_basis.body.append(button.button)
parsed_html_basis.body.append(button_javascript_tag)

# create directory for export and export file
export_directory = os.path.join(os.path.dirname(args.input[0]), 'html')
if not os.path.exists(export_directory):
    os.makedirs(export_directory)
with open(os.path.join(export_directory, args.input[0].replace('.xml', '.html').split(os.sep)[-1]), 'w') as fh:
    fh.write(parsed_html_basis.prettify())