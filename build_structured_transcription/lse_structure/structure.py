# -*- coding: utf-8 -*-
import re
import json
from bs4 import BeautifulSoup

from lse_utils import lse_utils as lseUtils
from lse_classes import teiparse_class as teiParse
from lse_classes import altoparse_class as altoParse
from tqdm import tqdm

VERBOSE = False  # if True, triggers a series of execution messages


def slice_volume_in_chapters(list_of_pages: list) -> list:
    """Get a list of item and split it according to detected title pages.

    :param list_of_pages: list of PageParser objects
    :return: list of lists of PageParser objects
    """
    def get_type_of_chapter(chapter):
        for unit in chapter.content:
            if unit.has_title:
                chapter.type_of_chapter = unit.type_of_title
        return chapter

    list_of_chapters = []
    current_index = 0
    last_index = 0
    for page in list_of_pages:
        if page.has_title is True:
            sliced = lseUtils.slice_a_list(list_of_pages, current_index, last_index)
            if len(sliced) > 0:
                chapter = teiParse.ChapterHandler(content=sliced)
                list_of_chapters.append(get_type_of_chapter(chapter))
                last_index = current_index
        current_index += 1
    last_slice = teiParse.ChapterHandler(content=lseUtils.slice_a_list(list_of_pages, current_index, last_index))
    list_of_chapters.append(get_type_of_chapter(last_slice))
    return list_of_chapters


def classify_first_page(page) -> None:
    """Identify investigation title pages and classify them.

    :type page: PageHandler object
    :return:
    """
    def is_this_precis_de_monographie(top_lines: list) -> bool:
        """Look for specific pattern of "précis de monographie" title page in list of 5 tags

        :param top_lines: list of 5 "l" tags, normally the 5 top-est lines
        :return: True if found pattern, False otherwise
        """
        """What do we call "précis de monographie"?
        In Le Play's Monographies, volumes contain supplement to regular investigations which, for the most part, 
        more or less follow the same rules for lay out - despite variation from a series to another. Their title
        alwats starts with 'Précis de monographie ...".
        """
        found_pattern = False
        for top_line in top_lines:
            if top_line.string:
                if not lseUtils.they_are_different("précis de monographie", top_line.string, threshold=7)[0]:
                    if VERBOSE:
                        print("\n[INFO]: {} is a 'précis de monographie' title page".format(page.name))
                    found_pattern = True
                    break
        return found_pattern

    def is_this_supplement(top_lines: list) -> bool:
        """Look for specific pattern of "supplement" title page in list of 2 tags

        :param top_lines: list of 2 "l" tags, normally the 2 top-est lines
        :return: True if found pattern, False otherwise
        """
        """What do we call supplement?
        In Le Play's Monographies, volumes contain 2 notable exceptionnal supplementary investigations 
        (enquêtes sociologiques) which we identify by their unique title :
        - La Société Générale
        - Usine Hydraulique"""
        marker_supplement = []
        for top_line in top_lines:
            if top_line.string:
                if top_line.string == "A":
                    marker_supplement.append(True)
                elif not lseUtils.they_are_different("LA SOCIÉTÉ GÉNÉRALE", top_line.string)[0]:
                    marker_supplement.append(True)
                elif not lseUtils.they_are_different("USINE HYDRAULIQUE", top_line.string)[0]:
                    marker_supplement.append(True)
        if len(marker_supplement) == 2:
            if VERBOSE:
                print("\n[INFO]: {} is a 'supplément' title page".format(page.name))
            return True
        else:
            return False

    def is_this_regular_investigation(lines: list) -> bool:
        """Look for specific pattern of regular investigation page in list of tags

        :param lines: list of "l" tags
        :return: True if found pattern, False otherwise
        """
        """What do we call regular investigation?
        In Le Play's Monographies, volumes contain investigations (enquêtes sociologiques) which, for the most part, 
        are following the same rules for lay out - despite variation from a series to another.
        """
        test_results = []
        for line in lines:
            if line.string:
                if not lseUtils.they_are_different("observations préliminaires", line.string)[0]:
                    test_results.append(True)
                elif not lseUtils.they_are_different("définissant la condition des divers membres de la famille",
                                                     line.string)[0]:
                    test_results.append(True)
                elif not lseUtils.they_are_different("définition du lieu, de l'organisation industrielle", line.string)[0]:
                    test_results.append(True)
                elif not lseUtils.they_are_different("§ 1ᵉʳ. — état du sol, de l'industrie et de la population", line.string)[0] or not lseUtils.they_are_different("état du sol, de l'industrie et de la population", line.string)[0]:
                    test_results.append(True)
        if len(test_results) > 2:  # is this too low? should it be 3?
            if VERBOSE:
                print("\n[INFO]: {} is an investigation title page!".format(page.name))
            return True
        return False

    if len(page.handler.find_all("graphic")) == 0:
        lines = page.handler.find_all("l")
        if is_this_supplement(lines[:2]) is True:  # this is an identified exception
            # expect to fine "A" and either "usine hydraulique" either "la société générale" in top 2 lines of page
            page.has_title = True
            page.type_of_title = "supplement"
        elif is_this_precis_de_monographie(lines[:5]) is True:  # this is an identified exception
            # expect to find "précis de monographie" in top 5 lines of page (could even be top 3)
            page.has_title = True
            page.type_of_title = "precis"
        elif is_this_regular_investigation(lines) is True:  # this is a regular investigation title page
            page.has_title = True
            page.type_of_title = "investigation"
        else:
            if VERBOSE:
                print("[TEST]: This is NOT a title page")
    else:
        if VERBOSE:
            print("[TEST]: This is NOT a title page")
    return  # should this return something?


def titles_in_front_and_back_matter(pages: list) -> list:
    """Process pages to locate title pages in front and back matters (which are chapters before and after investigations
    chapters."""
    # THIS STEP CAN BE IMPROVED BY INCLUDING CLUSTERING
    def is_this_front_matter_title(bingo: dict, all_lines: list):
        """Process a list of string comparing them to GT to see they are the content of a chapter's title page."""
        found_title = False
        if len(all_lines) > 0:
            for line in all_lines[0]:
                for title, status in bingo.items():
                    if status is False:
                        if VERBOSE:
                            print("[TEST]: comparing '{}' to '{}'".format(line.string, title))
                        if not lseUtils.they_are_different(title, line.string, threshold=3)[0]:
                            bingo[title] = True
                            found_title = True
                            if VERBOSE:
                                print("[TEST]: Found '{}' in {}".format(title, line.string))
        return bingo, found_title

    # N.B.: this GT dictionnary is not case sensitive.
    front_back_bingo = {
        "AVERTISSEMENT": False,
        "INSTITUTION": False,
        "DéFINITIONS": False,
        "EXPLICATION": False,
        "RAPPORT": False,
        "LISTE GéNéRale": False,
        "INSTRUCTION": False,
        "QUESTION DE LA FAMILLE": False,
        "SOMMAIRE": False,
        "Table alphabétique": False,
        "Table des matières": False,
        "Liste": False,
        "Errata": False,  # can also be Erratum but oh, well
        "Société Internationale": False,
        "Société d'économie sociale": False,
        "Liste générale": False,
        "Oeuvres de F. Le Play": False
    }

    for page in pages:
        lines = page.handler.find_all("l")
        fm_bingo, found = is_this_front_matter_title(front_back_bingo, lines)
        if found is True:
            page.has_title = True
            page.type_of_title = "unspecified"
    return pages


def get_page_and_headers(chapters: list) -> list:
    """Process pages to extract their 'header' content (section/chapter title and page number).

    :param chapters: list of list of pageHandler objects
    :return: list of list of pageHandler objects with info on headers and page numbers.
    """
    def compare_headers(line: str, context="reg") -> bool or str:
        """Test a string to see if it matches GT from reference data."""
        if context == "reg":
            ref = ref_headers["headers"]
        elif context == "sup":
            ref = ref_headers["headers_exception"]
        else:
            ref = None
        header = False
        for test in ref:
            if len(test) < 10:
                threshold = 3
            else:
                threshold = 10
            if not lseUtils.they_are_different(test, line, threshold=threshold)[0]:
                header = test
        return header

    # load reference data
    with open("./reference_headers.json", "r") as fh:
        ref_headers = json.load(fh)

    for chapter in tqdm(chapters, desc="Retrieving headers", unit="chapter", ncols=100, leave=True):
        supplement_context = False
        if chapter.type_of_chapter == "supplement":
            supplement_context = True
        for page in tqdm(chapter.content, unit="page", ncols=60, leave=False):
            if page.has_title is False:
                top_lines = [line for line in page.handler.find_all("l") if line.string][:3]
                for line in top_lines:
                    if line.string:
                        if len(line.string.replace(" ", "")) < 62:
                            # headers are not longer than this, even including page numbers
                            if line.string.isdigit():
                                page.number = int(line.string)
                                trash = line.extract()
                            else:
                                # page number and headers can be read in the same line, they are easy to spot when the
                                # header starts with "N°" so we may as well keep them in memory
                                if line.string.split("N°")[0].replace(" ", "").isdigit():
                                    page.number = int(line.string.split("N°")[0].replace(" ", ""))
                                if supplement_context:
                                    header = compare_headers(line.string, context="sup")
                                    if header:
                                        page.header = header  # we keep the correct version here
                                        trash = line.extract()
                                if not page.header or len(page.header) == 0:
                                    header = compare_headers(line.string)
                                    if header:
                                        page.header = header  # we keep the correct version here
                                        trash = line.extract()
    return chapters


def build_pagination(chapters: list) -> list:
    """Build the physical pagination of a volume as completely as possible and return the volume."""
    def is_this_coherent(orig: list, submission: list, min_similarity=0.3) -> tuple:
        """Measure the similarity between two lists of page numbers. Return True is similar."""
        differences = 0
        for i in range(len(submission)):
            if submission[i] != orig[i]:
                # we ignore difference on unrecognized page numbers
                if orig[i] > 0:  # unrecognized page numbers == -1 or 0
                    differences += 1
        # acceptable variation is set to a limit of 50%
        difference_rate = differences/len(submission)
        if difference_rate >= min_similarity:
            return False, difference_rate
        else:
            # submission and original match, then return True
            return True, difference_rate

    def set_limit(recognized: list) -> int:
        """Count integers in a list of integers"""
        counter = 0
        for item in recognized:
            if item > 0:
                counter += 1
        return counter - 1

    def build_submission(anchor: int, original_pagination: list) -> list:
        """Build a series of page numbers using an anchor as a starting point extended left and right.

        :param anchor: index of the item in the original_pagination used as a starting point
        :param original_pagination: incomplete list of page numbers
        :return: newly calculate list of page numbers"""
        submission = original_pagination[:]
        starting_point = submission[anchor]
        # build pagination before the anchor
        anchor_left = anchor -1
        built_page_number = starting_point - 1
        while anchor_left >= 0:
            submission[anchor_left] = built_page_number
            # page number won't go under 0
            if built_page_number > 0:
                built_page_number -= 1
            anchor_left -= 1
        # build pagination after the anchor
        anchor_right = anchor + 1
        built_page_number = starting_point + 1
        while anchor_right < len(original_pagination):
            submission[anchor_right] = built_page_number
            built_page_number += 1
            anchor_right += 1
        return submission

    def get_anchor(original_pagination: list, skip: int) -> bool or int:
        """Find index of a recognized page number after skipping a given number of 'found'"""
        index_tracker = 0
        counter_true = 0  # counts how many times we found a recognized page number
        while index_tracker < len(original_pagination):
            # unrecognized page numbers == 0 or -1
            if original_pagination[index_tracker] <= 0:
                index_tracker += 1
            else:
                if counter_true == skip:
                    return index_tracker
                else:
                    index_tracker += 1
                    counter_true += 1
        return False

    def echo_pagination(pages: list, submission: list) -> None:
        """Pass list of page numbers on to list of PageParser objects."""
        if len(submission) <= len(pages):
            items = len(submission)
        else:
            items = len(pages)
        for i in range(items):
            pages[i].number = submission[i]

    def build_a_new_pagination(original_list_of_page_numbers: list) -> bool or list:
        """Take a list of integers which are page numbers and fill the gap to create a continuous series."""
        max_try = set_limit(original_list_of_page_numbers)
        if max_try > 0:
            try_counter = 0
            anchor = get_anchor(original_list_of_page_numbers, skip=try_counter)
            if anchor:
                submission = build_submission(anchor, original_list_of_page_numbers)
                coherence, coherence_rate = is_this_coherent(original_list_of_page_numbers, submission)
                while not(coherence or try_counter <= max_try):
                    anchor = get_anchor(original_list_of_page_numbers, skip=try_counter)
                    submission = build_submission(anchor, original_list_of_page_numbers)
                    coherence, coherence_rate = is_this_coherent(original_list_of_page_numbers, submission)
                    try_counter += 1
                if coherence:
                    return submission
        return False

    def get_context(index: int, chapters: list, success_tracker: list, direction: str) -> tuple:
        """Get last or first page number in previous or next chapter in list and also get a status.
        If direction="prev", will return context *prior* to current chapter (index) and True if pagination worked
        If direction="next", will return context *after* current chapter (index) and True if pagination worked
        """
        if direction == "prev":
            index -= 1
            cursor = -1
        elif direction == "next":
            index += 1
            cursor = 0
        else:
            print('[WARNING]: in structure.get_context(): expected `prev` or `next` in arg. direction, got `{}`'.format(direction))
            return False, False
        try:
            page_number = [page.number for page in chapters[index].content][cursor]
            chapt_status = success_tracker[index]
        except IndexError:
            page_number = None
            chapt_status = False
        return page_number, chapt_status

    paginated_chapters = []  # list of processed chapters
    success_tracker = []  # for each chapter in chapters, there will be a true or a false in this list
    for chapter in chapters:
        original_list_of_page_numbers = [page.number for page in chapter.content]
        new_pagination = build_a_new_pagination(original_list_of_page_numbers)
        if new_pagination:
            echo_pagination(chapter.content, new_pagination)
            paginated_chapters.append(chapter)  # append ChapterHandler
            success_tracker.append(True)
        else:
            paginated_chapters.append(chapter)  # append ChapterHandler
            success_tracker.append(False)
    for status, chapter in zip(success_tracker, paginated_chapters):
        if status is False:
            original_list_of_page_numbers = [page.number for page in chapter.content]
            # why did this not work? There are several possible reasons:
            # there was not enough data to process but we can try using the context:
            # so we try to get the last page nb before this chapter and the first page nb after it
            # only if there is something before and/or something after
            if paginated_chapters.index(chapter) > 0 and paginated_chapters.index(chapter) > len(paginated_chapters) -1:
                index = chapters.index(chapter)
                # None if no pagination, False if not recognized
                prev_pagination, prev_status = get_context(index, chapters, success_tracker, direction="prev")
                next_pagination, next_status = get_context(index, chapters, success_tracker, direction="next")
                # we proceed if the previous and next chapters were correctly paginated, which might not always be the case
                # this condition could be improved by deciding on following either the previous or the following pagination
                # or by taking a broader context ex: 2 chapters before/after
                if prev_status and next_status:
                    if (next_pagination - prev_pagination) == len(original_list_of_page_numbers) + 1:
                        number = prev_pagination
                        for page in chapter.content:
                            number += 1
                            page.number = number
            # ignoring the problem when:
            # the "chapter" is actually all the front pages before the first chapter:
            # the "chapter" is the last chapter ?
            # this last one will cause issues: the case where there are liminary pages excluded from pagination in the chapter
    return chapters


def sort_out_tables_from_budget_sections(chapters: list) -> list:
    """Process a list of pageHandlers to find pages containing budget tables and remove quite drastically their content:
    they only contain a table (which we ignore) and sometimes this table was not well classified by the segmenter.

    :param chapters: list of list of pageHandler objects
    :return: list of list of pageHandler objects
    """
    def where_do_budgets_start(list_of_pages: list) -> bool or int:
        """Process a list of pages to find the beginning of the budget section (starting with
        'BUDGET DES RECETTES DE L'ANNÉE') - limiting search to the top 3 lines."""
        for page in list_of_pages:
            for line in page.handler.find_all("l")[:3]:
                if line.string:
                    if not lseUtils.they_are_different("BUDGET DES RECETTES DE L'ANNÉE", line.string, threshold=5)[0]:
                        #print("[TEST]: start of budget pages : {}".format(page.name))
                        return list_of_pages.index(page)
        return False

    def where_do_notes_start(list_of_pages: list) -> bool or int:
        """Process a list of pages to find the begining of the last section of an investigation (starting with
        'NOTES.' or 'ÉLÉMENTS DIVERS DE LA CONSTITUTION SOCIALE.') - limiting search to the top 3 lines."""
        for page in list_of_pages:
            for line in page.handler.find_all("l")[:3]:
                if line.string:
                    if not lseUtils.they_are_different("NOTES.", line.string, threshold=3)[0] or not lseUtils.they_are_different("ÉLÉMENTS DIVERS DE LA CONSTITUTION SOCIALE.", line.string, threshold=5)[0]:
                    # print("[TEST]: start of notes pages : {}".format(page.name))
                        return list_of_pages.index(page)
        return False

    def get_title_of_budget_table(line: BeautifulSoup) -> tuple:
        """Test a string to see if it matches the title of a budget table."""
        result = False, None, None
        ground_truth = ["BUDGET DES RECETTES DE L'ANNÉE.", "BUDGET DES DÉPENSES DE L'ANNÉE.",
                        "BUDGET DES RECETTES DE L'ANNÉE (SUITE).", "BUDGET DES DÉPENSES DE L'ANNÉE (SUITE).",
                        "COMPTES ANNEXÉS AUX BUDGETS."]
        for gt in ground_truth:
            if line.string:
                if not lseUtils.they_are_different(gt, line.string, threshold=5)[0]:
                    result = True, gt, line
                    break
        return result

    for chapter in chapters:
        if chapter.type_of_chapter == "investigation":
            budget_start = where_do_budgets_start(chapter.content)  # budget_start is an index
            budget_stop = where_do_notes_start(chapter.content)  # budget_stop is an index
            for page_with_budget in chapter.content[budget_start:budget_stop]:
                control = False
                for line in page_with_budget.handler.find_all("l")[:3]:
                    # here we get True/False, the model of title, the <l> tag and it content
                    control, correct_title, budget_title = get_title_of_budget_table(line)
                    if control:
                        break
                if control:  # if we found a title for this page of budget...
                    # we correct the title of the table while we're at it
                    budget_title.string = correct_title
                    saved_paragraph = budget_title.parent.extract()
                else:
                    saved_paragraph = False
                    # we remove all the other paragraphs (that is, all the other lines)
                for paragraph in page_with_budget.handler.find_all("p"):
                    trash = paragraph.extract()
                    # then we insert the paragraph we set aside as the very first item in the content of the tag
                    # and when no graphic was recognized on the page, we add one
                if len(page_with_budget.handler.div.contents) == 0:
                    if saved_paragraph:
                        page_with_budget.handler.div.append(saved_paragraph)
                    graphic_element = page_with_budget.handler.new_tag("graphic", TYPE="table")
                    page_with_budget.handler.div.append(graphic_element)
                    # add VPOS, HPOS, WIDTH and HEIGHT as well as ID attributes ---------------------- FLAG
                    # these coordinates can be calculated using the page's dimension and the top paragraph coordinates.
                    # ID are like this "tbl_#_#"
                else:
                    if saved_paragraph:
                        page_with_budget.handler.div.contents[0].insert_before(saved_paragraph)
    return chapters


# ============================================ TEI ============================================ #

def remove_lb_tags(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process a TEI XML tree to remove all <lb> elements."""
    for lb in tei_tree.find_all('lb'):
        trash = lb.extract()
    return tei_tree


def resolve_hyphenations(tei_tree: BeautifulSoup) -> BeautifulSoup:
    def resolve_hyphenations_in_paragraph(paragraph: BeautifulSoup) -> None:
        paragraph_content = ''
        for line in paragraph.contents:
            paragraph_content = paragraph_content + line.strip()
            if len(paragraph_content) > 0:
                if paragraph_content[-1] == '-':
                    paragraph_content = paragraph_content[:-1] + '¬'
                else:
                    paragraph_content = paragraph_content + ' '
            else:
                paragraph_content = paragraph_content + ' '
        paragraph.clear()
        paragraph.append(paragraph_content)

    def resolve_hyphenations_in_page(page):  # NOT Finished! - - - - - - - - - - - - - - - - - - - - - - - - - - - FLAG!
        all_paragraphs_here = page.find_all('p')
        for child in page.contents:
            if child.name == 'p':
                if len(child.string.strip()) > 0:
                    if child.string.strip()[-1] == '¬':
                        # this is an interrupted paragraph
                        if child == all_paragraphs_here[-1]:
                            # then the other part of the paragraph is not on this page but on the next one
                            2+2
                            # print(page.attrs)
                            # WHAT DO WE DO NOW? = = = = = = = = = = = = = = = = = = = = = = = = = = = = FLAG!
                    elif child.string.strip()[-1] != '.':
                        # this is probably an interrupted paragraph but it is also probably part of a title or a weird table
                        1 + 1
                        #print("[PLACEHOLDER]")
                        # WHAT DO WE DO NOW? = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = FLAG!

    for p in tei_tree.body.find_all('p'):
        resolve_hyphenations_in_paragraph(p)
    for page in tei_tree.body.find_all('div'):
        if page.attrs['type'] == 'page':
            resolve_hyphenations_in_page(page)
    return tei_tree


def build_chapters_id(chapters: BeautifulSoup) -> BeautifulSoup:
    """Process a TEI XML tree to create ids for chapters and add them into their @ID attribute."""
    counter = 1
    for div in chapters.find_all('div'):
        if div.attrs['type'] == 'chapter':
            div.attrs['ID'] = 'chapt_{}'.format(counter)
            counter += 1
    return chapters


def build_pagecontent_ids(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process a TEI XML tree to create ids for blocks contained in a page and add them into their @ID attribute."""
    for chapter_div in tei_tree.find_all('div'):
        if chapter_div.attrs['type'] == 'chapter':
            graphic_counter = 1
            paragraph_counter = 1
            chapter_id_nb = chapter_div.attrs['ID'].split('_')[-1]
            for page in chapter_div.contents:
                page_number = page.attrs['PAGENB']
                for block in page.contents:
                    if block.name:
                        if block.name == 'graphic':
                            if block.attrs['TYPE'] == 'table':
                                graph_type = 'tble'
                            elif block.attrs['TYPE'] == 'illustration':
                                graph_type = 'illu'
                            block.attrs['ID'] = '{}_{}_{}_{}'.format(graph_type, chapter_id_nb, page_number, graphic_counter)
                            graphic_counter += 1
                        elif block.name == 'p':
                            block.attrs['ID'] = 'para_{}_{}_{}'.format(chapter_id_nb, page_number,paragraph_counter)
                            paragraph_counter += 1
    return tei_tree


def transform_pagediv_into_pb(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process TEI XML tree to transform <div @type="page">{page content}</div> into <pb/>{page content}... ."""
    # for some reason iteration on div.contents was not iterating on every content
    for div in tei_tree.find_all('div'):
        if div.attrs['type'] == 'page':
            newly_shaped_page_content = div.contents[:]
            div.clear()
            div.name = 'pb'
            anchor = div
            for item in newly_shaped_page_content:
                anchor.insert_after(item)
                anchor = item
    return tei_tree


def create_zones_in_facsimile(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process a TEI XML tree to create facsimiles, zones and bounds between text and facsimiles items.

    :param tei_tree: parsed TEI XML tree
    :return: parsed TEI XML tree with zone and bounds between facsimiles and text.
    """
    def create_zone(block: BeautifulSoup) -> BeautifulSoup:
        """Create a <zone> element with correct attributes for a given facsimile's zone."""
        if 'HPOS' in block.attrs and 'VPOS' in block.attrs and 'WIDTH' in block.attrs and 'HEIGHT' in block.attrs:
            new_zone = tei_tree.new_tag("zone",
                                    ulx=str(block.attrs['HPOS']),
                                    uly=str(block.attrs['VPOS']),
                                    lrx=str(int(float(block.attrs['HPOS']) + float(block.attrs['WIDTH']))),
                                    lry=str(int(float(block.attrs['VPOS']) + float(block.attrs['HEIGHT']))))
        else:
            new_zone = tei_tree.new_tag("zone")
        if block.name == 'p':
            new_zone.attrs['rendition'] = 'paragraph'
            blocktype = 'p'
        elif block.name == 'graphic':
            blocktype = 'g'
            if block.attrs['TYPE'] == 'illustration':
                new_zone.attrs['rendition'] = 'illustration'
            elif block.attrs['TYPE'] == 'table':
                new_zone.attrs['rendition'] = 'table'
        else:
            blocktype = ''
        identifier = '{}_{}_{}'.format(block.parent.attrs['facs'][1:], blocktype,
                                       lseUtils.get_iterator_length(block.previous_siblings) + 1)
        new_zone['xml:id'] = identifier
        return new_zone

    for page in tei_tree.body.find_all('div', type='page'):
        # we get the page's facs' xml:id by removing '#' from the value of @facs
        # we can't simply look for tei_tree.find_all('facsimile', xml:id=page.attrs['facs'][1:] because of the namespace
        page_facs = [f for f in tei_tree.find_all('facsimile') if f.attrs['xml:id'] == page.attrs['facs'][1:]][0]
        location_of_insertion = [z for z in page_facs.surface.find_all('zone', rendition='printspace')][0]
        for content in page.contents:
            if content.name:
                new_zone = create_zone(content)
                location_of_insertion.append(new_zone)
                content.attrs['facs'] = '#{}'.format(new_zone.attrs['xml:id'])
                # delete HPOS VPOS WIDTH and HEIGHT - or maybe not yet...
    return tei_tree


def delete_attributes_in_paragraphs(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process <p> element and delete unnecessary attributes."""
    for tag in tei_tree.body.find_all('p'):
        del tag['HEIGHT']
        del tag['WIDTH']
        del tag['HPOS']
        del tag['VPOS']
    return tei_tree


def delete_attributes_in_graphics(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process <graphic> elements and delete unnecessary attributes."""
    for tag in tei_tree.body.find_all('graphic'):
        del tag['HEIGHT']
        del tag['WIDTH']
        del tag['HPOS']
        del tag['VPOS']
        tag['type'] = tag['TYPE']
        del tag['TYPE']
    return tei_tree


def delete_attributes_in_pagebeginings(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process <pb> elements and delete unnecessary attributes."""
    for tag in tei_tree.body.find_all('pb'):
        del tag['HEIGHT']
        del tag['WIDTH']
        del tag['img']
        del tag['type']
        del tag['ID']
        tag['n'] = tag['PAGENB']
        del tag['PAGENB']
    return tei_tree


def find_logical_structure_in_investigations(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Process a TEI XML tree to identify logical ensembles such as sections, subsections and subsubsections.

    :param tei_tree: parsed TEI XML tree
    :return: parsed TEI XML tree with integration of logical structure
    """
    """N.B.: This step will only look for the basic structure one can find in regular investigation chapters
    It needs to be generalized by relying not on a provided list of GT titles but rather by taking into account
    physicial clues (such as space between paragraphs, size of texte, etc).
    
    More robust control needs to be added to this step by veryfying the logical flow of titles -> use TOC and headers
    for control.
    
    The integration of the logical structure in the XML needs to be corrected: embedding of the first item for a given
    level is flawed."""
    def find_section_titles(submission: str) -> tuple:
        """Compare line of text with a list of possible (sub(sub))titles."""
        with open("./reference_sections.json", "r") as fh:
            ref_sections = json.load(fh)
        for key in ref_sections.keys():
            for test in ref_sections[key]:
                if len(test) < 6:
                    threshold = 3
                else:
                    threshold = 8
                if not lseUtils.they_are_different(test, submission, threshold=threshold)[0]:
                    # print("[YEAY]: '{}' and '{}' are the same".format(test, submission))
                    return key, test
        # print("[INFO]: '{}' is not a title portion".format(submission))
        return False, False

    def find_levels(tei_tree: BeautifulSoup) -> BeautifulSoup:
        """Identify beginning of logical (sub(sub))sections and give them a @level attribute."""
        for par in tei_tree.find_all('p'):
            if len(par.string) < 100 and not par.string.replace(" ", "").isdigit():
                # if text in paragraph is longer 100 char that it is most likely not a title section
                level, correct = find_section_titles(par.string)
                if correct:
                    # print("[INFO]: found level '{}' for '{}' because it looks like '{}' [{}]".format(level, par.string, correct, par.attrs['facs']))
                    par.string = correct
                    par['level'] = level
        return tei_tree

    def cut_a_div_into_logical_chunks(div: BeautifulSoup, targetted_level='') -> list:
        """Process content of a <div> element and create logical blocks with @level corresponding to targetted level."""
        if targetted_level != 'section' and targetted_level != 'sub_section' and targetted_level != 'sub_sub_section':
            print("[WARNING]: invalid expression for attribute targetted_level in function 'cut_a_div_in_sections()'")
            return [div]
        list_of_sections = []
        current_index = 0
        last_index = 0
        all_blocks_in_div = div.contents
        for block in all_blocks_in_div:
            if 'level' in block.attrs:
                if block['level'] == targetted_level:
                    list_of_sections.append(lseUtils.slice_a_list(all_blocks_in_div, current_index, last_index))
                    last_index = current_index
            current_index += 1
        list_of_sections.append(all_blocks_in_div[last_index:])
        return list_of_sections

    def apply_sections_and_sub_sections(tei_tree: BeautifulSoup, level='') -> BeautifulSoup:
        """Process XML tree to find and render different logical levels (chapter/section/subsection/subsubsection)."""
        if level == 'sub_sub_section':
            context = 'sub_section'
        elif level == 'sub_section':
            context = 'section'
        elif level == 'section':
            context = 'chapter'
        else:
            context = False
            print("[INFO]: invalid expression for attribute 'level' in function 'apply_sections_and_sub_sections()'")
            return tei_tree

        for ensemble in tei_tree.find_all('div', type=context):
            sections_in_this_ensemble = cut_a_div_into_logical_chunks(ensemble, targetted_level=level)
            # the very first section might not need to be wrapped in a div:
            if 'level' not in sections_in_this_ensemble[0][0].attrs:
                sections_in_this_ensemble = sections_in_this_ensemble[1:]
            # elif sections_in_this_ensemble[0][0].attrs['level'] != level:
            #    sections_in_this_ensemble = sections_in_this_ensemble[0][1:] + sections_in_this_ensemble[1:]
            counter = 1
            for section in sections_in_this_ensemble:
                if len(str(counter)) == 3:
                    formatted_counter = counter
                else:
                    formatted_counter = str(counter)
                    for i in range(3 - len(str(counter))):
                        formatted_counter = "0" + formatted_counter
                div_wrapping_the_section = tei_tree.new_tag('div', type=level, n=formatted_counter)
                if len(section) > 0:
                    section[0].wrap(div_wrapping_the_section)
                    if len(section) > 1:
                        for tag in section[1:]:
                            div_wrapping_the_section.append(tag)
                counter += 1
        return tei_tree

    tei_tree = find_levels(tei_tree)
    tei_tree = apply_sections_and_sub_sections(tei_tree, level='section')
    tei_tree = apply_sections_and_sub_sections(tei_tree, level='sub_section')
    tei_tree = apply_sections_and_sub_sections(tei_tree, level='sub_sub_section')
    return tei_tree


def create_heads_in_sections(tei_tree):
    """Change <p @level="{$level}"> elements into <head @type="{$level}">."""
    for tag in tei_tree.body.find_all('p'):
        if 'level' in tag.attrs:
            tag.name = 'head'
            tag['type'] = tag['level']
            del tag['level']
    return tei_tree


def normalize_id_attributes(tei_tree: BeautifulSoup) -> BeautifulSoup:
    """Change @ID into TEI-esque @xml.id."""
    for tag in tei_tree.body.find_all():
        if 'ID' in tag.attrs:
            tag['xml:id'] = tag['ID']
            del tag['ID']
    return tei_tree


def handle_footnotes(tei_tree: BeautifulSoup) -> BeautifulSoup:
    for page in tei_tree.body.find_all('div'):
        # logically HERE we should exclude from the texte "title/front pages" because they only contain titles...
        # ... and a lot of gaps, but never contain footnote sections !
        # --------------------------------------------------
        if page.attrs['type'] == 'page':
            # print(page.attrs['ID'])
            eligible_blocks = []
            for element in page.contents:
                # We sort blocks on which we can build our tests (they are XML tags and have a VPOS attribute)
                # this should exclude strings:
                if element.name and 'VPOS' in element.attrs:
                    eligible_blocks.append(element)
            # We spot significant gaps in the page which can mark the beginning of a footnote section
            # although they can also mark the end of a section, so we will have tests later to sort
            # footnote sections from the rest
            n = 0
            remains = []
            for element in eligible_blocks[:-1]:
                # is there a significant gap between 2 blocks? if yes then we split the list of blocks so as to only
                # keep the second half, the one part starting AFTER the significant gap!
                gap = (float(eligible_blocks[n + 1].attrs['VPOS']) - (float(element.attrs['VPOS']) + float(element.attrs['HEIGHT']))) / float(page.attrs['HEIGHT'])
                if gap > 0.007:
                    # this is not perfect because in VERY FEW CASES, there can be a gap within the footnote section:
                    remains = eligible_blocks[n + 1:]
                n += 1
            # footnotes section always and only starts with a paragraph, no figure
            remains_paragraphs = [element for element in remains if element.name == 'p']
            # normally, a footnote section doesn't go higher than the middle of the page
            bottom_paragraphs = []
            for element in remains_paragraphs:
                # but there is a couple exceptions, namely on the last page of a chapter, where it can start higher
                # HERE we need to add another test for such exception.
                # where limit == another value.
                limit = int(float(page.attrs['HEIGHT']) / 2.0)
                if int(element.attrs['VPOS']) > limit:
                    bottom_paragraphs.append(element)
            # testing if block is aligned on the left side of page
            aligned_on_left_paragraphs = []
            for element in bottom_paragraphs:
                left_margin_ratio = float(element.attrs['HPOS']) / float(page.attrs['WIDTH'])
                if 0.16 >= left_margin_ratio >= 0.070:
                    aligned_on_left_paragraphs.append(element)

            # testing density
            density_test = 0
            for element in aligned_on_left_paragraphs:
                nb_of_lines_in_here = len(element.find_all('lb'))
                average_line_height = float(element.attrs['HEIGHT']) / float(nb_of_lines_in_here)
                ratio = average_line_height / float(page.attrs['HEIGHT'])
                if ratio <= 1.5:
                    density_test += 1
            if density_test == len(aligned_on_left_paragraphs) and len(aligned_on_left_paragraphs) > 0:
                # tester si la note commence par "\d.?" - C'est le pattern le plus commun.
                if re.match(re.compile(r"^\d\.? "), aligned_on_left_paragraphs[0].get_text().strip()):
                    # print(page.attrs['ID'])
                    for para in aligned_on_left_paragraphs:
                        # print(para.get_text())
                        trash = para.extract()  # this is the ultimate goal of this
                        # although at some point we also want to reinject them in the texte as notes.
                        # this still need much improvement !
    return tei_tree


def remove_empty_p_in_page(list_of_pageparsers: list) -> list:
    """Remove <p> tags from tree in it's empty."""
    for page in list_of_pageparsers:
        paragraphs = page.handler.find_all("p")
        for paragraph in paragraphs:
            if len(paragraph.contents) == 0:
                trash = paragraph.extract()
    return list_of_pageparsers
