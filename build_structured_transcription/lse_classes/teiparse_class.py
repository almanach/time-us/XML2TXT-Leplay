# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import os

class PageParser():
    """PageParser handles an an image, an ALTO XML representation of segmentation and an TEI XML transcription on top of
    additionnal metadata: image file name, number of page, header content, features such as emptiness or titledness and
    in such case classification of the title."""
    def __init__(self, image, alto, handler, has_title=False, type_of_title=None, is_empty=False, number=-1, header=None):
        self.name = image.replace(".tif", "").split(os.sep)[-1]
        self.image = image # original image file name
        self.alto = alto # original ALTO file
        self.handler = handler # TEI soup
        self.has_title = has_title
        self.type_of_title = type_of_title
        self.is_empty = is_empty
        self.number = number
        self.header = header


class ChapterHandler():
    """ChapterHandler handles a list of PageParser objects with a few additional metadata (title and type)."""
    def __init__(self, content, type_of_chapter=None, title=None):
        self.content = content  # list d'objets PageParser
        self.type_of_chapter = type_of_chapter  # 'investigation', 'toc', 'precis', etc.
        self.title = title  # if identified title


class TeiParser():
    """TeiParser handles a TEI XML tree (soup) associated to modified XML trees (one per page) and a title."""
    def __init__(self, title, originals, soup=False):
        self.title = title
        self.originals = originals # list of XML files gathered into a volume
        self.soup = soup

    def make_content(self, volume):
        """Create the soup: basic TEI header + concatenation of page transcriptions (list of <div>)"""
        def create_tei_base():
            """Return the essential of an TEI XML tree."""
            tei_base_as_string = """<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0"><teiHeader><fileDesc><titleStmt><title/></titleStmt><publicationStmt><p/></publicationStmt><sourceDesc><p/></sourceDesc></fileDesc></teiHeader><text><body></body></text></TEI>"""
            return BeautifulSoup(tei_base_as_string, "xml")

        tei_main = create_tei_base()
        tei_body = tei_main.find_all("body")
        for page in volume:
            tei_body[0].append(page.handler)
