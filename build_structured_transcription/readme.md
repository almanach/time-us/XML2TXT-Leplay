# LSE Pipeline

Plugging Transkribus and Kraken together to extract text and structure.

## Requirements

- beautifulsoup4==4.7.1
- bs4==0.0.1
- lxml==4.3.3

## ALTO2JSON

`alto2json.py` is used to create JSON files containing line segments for Kraken, extracted from an ALTO XML file.