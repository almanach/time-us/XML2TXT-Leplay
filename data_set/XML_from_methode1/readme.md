# Test set description

## XML_from_sharedocsFineReader

### What is this test set?
These XML files are the result of text extractions from PDF files corresponding to the 14 volumes of *Les Ouvriers des Deux Mondes* available on the Internet Archives : https://archive.org/search.php?query=ouvriers%20des%20deux%20mondes

The OCR was executed using the implementation of ABBYY's FineReader in Huma-Num's sharedocs. It was set with the "toXML" option for French text.

### testfile.xml
This is just a modified lighter example of the XML files contained in XML_from_sharedocsFineReader/ used for tests.

## Original digitizations

### First series (1 to 46)

| volume | URL                                                      | description                       |
| :----: | :------------------------------------------------------- | :-------------------------------- |
| I      | https://archive.org/details/lesouvriersdesde01sociuoft   | 1 to 9 - 1857                     |
| II     | https://archive.org/details/lesouvriersdesde02sociuoft   | 10 to 19 - 1858                   |
| III    | https://archive.org/details/lesouvriersdesde03sociuoft   | 20 to 28 - 1861                   |
| IV     | https://archive.org/details/lesouvriersdesde04sociuoft   | 29 to 37 - 1862                   |
| V      | https://archive.org/details/lesouvriersdesde05sociuoft   | 38 to 46 - 1885                   |

### Second series (47 to 91)
| volume | URL                                                      | description                       |
| :----: | :------------------------------------------------------- | :-------------------------------- |
| I      | https://archive.org/details/s2lesouvriersdes01sociuoft   | 47 to 55 + 48*bis* - 1887         |
| II     | https://archive.org/details/s2lesouvriersdes02sociuoft   | 56 to 64 + 57*bis*, 59*bis*- 1890 |
| III    | https://archive.org/details/s2lesouvriersdes03sociuoft   | 65 to 72 + 66*bis*, 66*ter*- 1892 |
| IV     | https://archive.org/details/s2lesouvriersdes04sociuoft   | 73 to 81 - 1895                   |
| V      | https://archive.org/details/2serlesouvriersde05sociuoft  | 82 to 91 + 90*bis*- 1899          |

### Third series (92 to 108)

| volume | URL                                                      | description                       |
| :----: | :------------------------------------------------------- | :-------------------------------- |
| I      | https://archive.org/details/lesouvriersdesde0108sociuoft | 92 to 99 + A, 92*bis*- 1904       |
| II     | https://archive.org/details/lesouvriersdesde0108sociuoft | 100 to 107 + A - 1908             |
| II     | https://archive.org/details/s3lesouvriersdes17soci       | 108 - 1908                        |
| II     | https://archive.org/details/lesouvriersdesde17sociuoft   | 108 - 1908                        |



